package com.yuyi;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.unionpay.acp.sdk.SDKConfig;

@SpringBootApplication
public class UnionpayApplication implements CommandLineRunner {

	public static void main(String[] args) {
		
		SpringApplication.run(UnionpayApplication.class, args);
		
	}

	@Override
	public void run(String... args) throws Exception {
		SDKConfig.getConfig().loadPropertiesFromSrc();
	}
}
