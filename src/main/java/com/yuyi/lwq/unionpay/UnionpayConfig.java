/**
 * 
 */
package com.yuyi.lwq.unionpay;

/**
 *
 * @author 李文庆
 * 2018年11月29日 下午1:42:53
 */
public class UnionpayConfig {

	/**商户号*/
	public static final String MER_ID = "777290058110048";
	
	public static String FRONT_URL = "http://localhost:8080/front.html";
	
	public static String BACK_URL = "http://lp520.51vip.biz:37963/union/notifyurl";
	
	
}
